function newMonitorState() {
	return {
		timeBarkingStarted: 0,
		timeOfLastBark: 0,
		timeOfLastEvent: 0,
		quietLogged: true
	};
}

function updateVisualization(microtime, analyser, monitorState) {
	var enoughTime = 5000;

	// TODO:
	// check for unlogged activity when we stop recording

    if($("#start-recording").hasClass("disabled")) {
    	var volume = calcVolume(analyser);
    	var threshold = parseFloat($("#db-threshold").val());

    	$("#current-volume").text(volume.toFixed(1));

    	var hasBeenQuiet = monitorState.timeOfLastBark < microtime - enoughTime;

    	var isQuiet = volume < threshold;	

    	// We've been quiet long enough to register the event
    	if(isQuiet && hasBeenQuiet && !monitorState.quietLogged) {
    		Events.insert({microtime:microtime, type:"barking stopped"});
    		monitorState.quietLogged = true;
    		var duration = parseInt(microtime - monitorState.timeBarkingStarted); 
    		var start = Date.now();
			ActivityLog.insert({"start": start, "duration": duration, "level": "high"});
			monitorState.timeOfLastEvent = microtime;
    	}

    	if(!isQuiet && hasBeenQuiet) {
    		Events.insert({microtime:microtime, type:"barking started"});
    		monitorState.quietLogged = false;
    		monitorState.timeBarkingStarted = microtime;
    		var duration = parseInt(microtime - monitorState.timeOfLastBark); 
    		var start = Date.now() - parseInt(microtime - monitorState.timeOfLastBark) + 5000;
			ActivityLog.insert({"start":start , "duration": duration, "level": "low"});
			monitorState.timeOfLastEvent = microtime;
    	}

    	if (!isQuiet) {
    		monitorState.timeOfLastBark = microtime;
    	}

		LocalLog.insert({microtime: microtime, value: volume}); 

    	requestAnimationFrame(function(microtime) {updateVisualization(microtime, analyser, monitorState)});
    }
}

function calcVolume(analyser) {
	// var dataArray = new Float32Array(analyser.fftSize);
	var dataArray = new Uint8Array(analyser.fftSize);
	analyser.getByteTimeDomainData(dataArray);
	var i;
	var volume = 0;
	for (i = 0; i < analyser.fftSize; i++) {
		volume += Math.abs(dataArray[i]-128)/analyser.fftSize;
	}

	var decibelRange = analyser.maxDecibels - analyser.minDecibels;
	volume = analyser.minDecibels + decibelRange * (volume/128);
	return volume;
}

function startAnalyzing() {
	navigator.getUserMedia = (navigator.getUserMedia ||
                        navigator.webkitGetUserMedia ||
                        navigator.mozGetUserMedia ||
                        navigator.msGetUserMedia);

	var FFT_SIZE = 1024;

    navigator.getUserMedia({audio:true}, function(stream) {

	    var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
	    var source   = audioCtx.createMediaStreamSource(stream);
	    var analyser = audioCtx.createAnalyser();
	    var samples  = new Uint8Array(FFT_SIZE);
	    analyser.fftSize = FFT_SIZE;
	    analyser.minDecibels = -50;
	    analyser.maxDecibels = -30;

	    source.connect(analyser);

	    updateVisualization(Date.now() * 1000, analyser, newMonitorState());

    }, function(err) {alert("Error: " + err)});

}

function clearHistory() {
	_.each(ActivityLog.find({},{fields:{_id:true}}).fetch(), function(activity) {
		ActivityLog.remove({_id:activity._id});
	});
}

function startRecording() {
	$("#start-recording").toggleClass('disabled');
	$("#stop-recording").toggleClass('disabled').data('start-time', Date.now());
	clearHistory();
	startAnalyzing();
}

function stopRecording() {
	$("#start-recording").toggleClass('disabled');

	var $stopBtn = $("#stop-recording");
	$stopBtn.toggleClass('disabled');
	var startTime = $stopBtn.data('start-time');
	var duration = -1;

	if ((typeof(startTime) !== 'undefined' || startTime !== null) && !isNaN(startTime)) {
		duration = Date.now() - startTime; 
	}

}

Template.current_activity.onRendered(function() {
	// Get context with jQuery - using jQuery's .get() method.
	var points = new Array(600);
	var labels = new Array(600);
	var i;
	var length = points.length;
	for(i=0; i < length; i++) {
		points[i] = -50;
		labels[i] = "";
	}

	labels[0] = "-1m"; 
	labels[150] = "-45s";
	labels[300] = "-30s";
	labels[450] = "-15s";
	labels[599] = "now";

	var ctx = $("#volume_chart").get(0).getContext("2d");
	var data = {

	    labels: labels,
	    datasets: [{
	            label: "My First dataset",
	            fillColor: "rgba(55,55,110,0.5)",
	            data: points
	        },
	    ]
	};	


	var options = {
		bezierCurve: false,
		animation: false,
		pointDot: false,
		scaleStartValue: -50,
		scaleOverride: true,
		scaleStepWidth: 2,
		scaleSteps: 5 
	};
	// This will get the first returned node in the jQuery collection.
	var myNewChart = new Chart(ctx).Line(data, options);	

	LocalLog.find().observe({
		added: function(event) {
			var points = myNewChart.datasets[0].points;
			//myNewChart.labels.push(event.value);
			var i = 0;
			for (; i < 599; i++) {
				points[i].value = points[i+1].value;
			}
			points[points.length-1].value = event.value;
			myNewChart.update();
		}
	});

	Events.find().observe({
		added:function(event) {
			console.log("Event: ", event);
		}
	})
});

Template.current_activity.events({
	"click #start-recording": function() {
		startRecording();
	},	

	"click #stop-recording": function() {
		stopRecording();
	}	
});

Template.current_activity.helpers({
	activity_log: function() {

		var fmtStart = function(start) { return new Date(start).toLocaleTimeString(); }; 
		var fmtDuration = function(duration) { 
			if (isNaN(duration) || duration < 0) {
				return "unknown";
			}

			var seconds = duration/1000;

			if (seconds < 60) {
				return parseInt(seconds) + " seconds";
			} else {
				return parseInt(minutes/60) + 1 + " minutes";
			}
		};

		return _.map(ActivityLog.find().fetch(), function(log) {
			return {
				start: fmtStart(log.start),
				duration: fmtDuration(log.duration),
				level: log.level
			}
		});
	}
});