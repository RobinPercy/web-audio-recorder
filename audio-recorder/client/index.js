Template.index.onRendered(function() {

	var audioContext  = new (window.AudioContext || window.webkitAudioContext)();

    function publishWAV(wavBlob) {
        var li = $(document.createElement("li"));
        var a = $(document.createElement("a")).text("Play").attr('href','#');
        li.append(a);
        a.on('click', function() {
            var newSource = audioContext.createBufferSource();
            var reader = new FileReader();
            reader.addEventListener("loadend", function() {

                audioContext.decodeAudioData(reader.result, function(data) {
            //        newSource.buffer = data;
            		debugger;
            		var d = data.getChannelData(0);
            		var a = new Uint8Array(d.buffer);
                    console.log("calling with", a);
			    	Meteor.call("saveFile", a);
			    	console.log("called");
            //        newSource.connect(audioContext.destination);
            //       newSource.start();
                });
            });
            reader.readAsArrayBuffer(wavBlob);
        });

        $("#wav-list").append(li);
    }

    navigator.getUserMedia = (navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia);

    navigator.getUserMedia( {audio:true},

            function (stream) {
                var audioCtx  = new (window.AudioContext || window.webkitAudioContext)();
                var source    = audioCtx.createMediaStreamSource(stream);

                var recorder = new Recorder(source, {workerPath: "recorderWorker.js", numChannels:1});

                $("#start").on('click', function(e) {
                    if (!$("#start").hasClass("disabled")) {
                        recorder.record();
                    }
                    $("#start").addClass('disabled');
                    $("#stop").removeClass('disabled');
                }).removeClass('disabled');

                $("#stop").on('click', function(e) {

                    if (!$("#stop").hasClass("disabled")) {
                        recorder.stop();
                    recorder.exportWAV(function(wav) {
                        publishWAV(wav);

                        $("#start").removeClass('disabled');
                        $("#stop").addClass('disabled');
                        recorder.clear();
                    });
                }
            });


        },

        function (err) {
            console.log('The following gUM error occured: ', err);
        }
    );
});

Template.index.events({
	"click #callit": function() {
		Meteor.call("saveFile");
	}
});